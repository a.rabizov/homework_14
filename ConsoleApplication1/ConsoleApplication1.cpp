﻿
#include <iostream>
#include <string>

int main()
{
    std::string name = "Ivan";
    std::cout << "String variable: " << name << std::endl;
    std::cout << "String length: " << name.length() << std::endl;
    std::cout << "String first character: " << name[0] << std::endl;
    std::cout << "String last character: " << name[name.length()-1] << std::endl;
}

